package com.yinhongbo.yrjy;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.List;

public class WelcomeActivity extends AppCompatActivity {
    private ViewPager welcomePager;
    private List<ImageView> imageViewList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        welcomePager = findViewById(R.id.welcome_pager);
        addImageView(R.drawable.welcome_1);
        addImageView(R.drawable.welcome_2);
        addImageView(R.drawable.welcome_3);
        addImageView(R.drawable.welcome_4);
        addImageView(R.drawable.welcome_5);
        addImageView(R.drawable.welcome_6);
        welcomePager.setAdapter(new PagerAdapter() {
            @Override
            public int getCount() {
                return imageViewList.size();
            }

            @Override
            public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
                return view == object;
            }

            //初始化 item 实例方法
            @Override
            public Object instantiateItem(ViewGroup container, int position) {
                container.addView(imageViewList.get(position));
                return imageViewList.get(position);
            }
            //item 销毁的方法
            @Override
            public void destroyItem(ViewGroup container, int position, Object object) {
                // 注销父类销毁 item 的方法，因为此方法并不是使用此方法
                // super.destroyItem(container, position, object);
                container.removeView(imageViewList.get(position));
            }
        });

    }

    private void addImageView(Integer domId)
    {
        ImageView imageView = new ImageView(this);
        imageView.setImageResource(domId);
        //拉伸图片
        imageView.setScaleType(ImageView.ScaleType.FIT_XY);
        imageViewList.add(imageView);
    }
}